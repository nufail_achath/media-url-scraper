const path = require("path");
const express = require("express");
var cors = require("cors");
const morgan = require("morgan");
const AppError = require("./utils/appError");
const globalErrorHandler = require("./controllers/errorController");
const scrapperRouter = require("./routes/scraperRoutes");
const viewRouter = require("./routes/viewRoutes");

const app = express();

//Server side rendering using pug template
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));

// Serving static files
app.use(express.static(path.join(__dirname, "public")));

// MIDLLEWARE
app.use(cors());
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(express.json());

// ROUTES

app.use("/api/v1/scrapers", scrapperRouter);
app.get("/", viewRouter);
//to handle routes otherthan specified routes
app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});
//global error handling
app.use(globalErrorHandler);
module.exports = app;

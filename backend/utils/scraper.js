const cheerio = require("cheerio");
const axios = require("axios");
const pattern = /^((http|https|ftp):\/\/)/;
const getSrcUrl = (url, src) => {
  if (!pattern.test(src)) {
    return url + "" + src;
  }
  return src;
};
const extractVideos = ($, url) => {
  return [
    ...new Set(
      $("video source") // inspect Sorce element under Video  element
        .map((index, elmnt) => {
          return $(elmnt).attr("src")
            ? {
                category: "video",
                type: $(elmnt).attr("type"),
                src: getSrcUrl(url, $(elmnt).attr("src")),
              }
            : {};
        })
        .toArray()
    ),
  ].filter((item) => Object.keys(item).length !== 0);
};

const extractImages = ($, url) => {
  return [
    ...new Set(
      $("img") //inspect images
        .map((indx, elmnt) => {
          return $(elmnt).attr("src")
            ? {
                category: "image",
                type: "image",
                src: getSrcUrl(url, $(elmnt).attr("src")),
              }
            : {};
        })
        .toArray()
    ),
  ].filter((item) => Object.keys(item).length !== 0);
};

exports.scrapeImageAndVideos = (weburls) => {
  return weburls.map((url) => {
    return axios
      .get(url)
      .then(({ status, data }) => {
        if (status == 200) {
          //connection to weburl
          const html = data;
          const $ = cheerio.load(html); // Initialize cheerio
          const images = extractImages($, url); //scrap images
          const videos = extractVideos($, url); //scrap videos
          return { url, images, videos };
        }
      })
      .catch((err) => console.log("err", err.message));
  });
};

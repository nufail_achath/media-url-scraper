const express = require("express");
const {
  checkScraperBody,
  scrapWebURLs,
  createScraper,
  getScrapedImagesAndVideos,
} = require("../controllers/scraperController");
const { isAuthenticated } = require("../controllers/authController");
const router = express.Router();
router
  .route("/:role")
  .post(isAuthenticated, checkScraperBody, scrapWebURLs, createScraper)
  .get(isAuthenticated, getScrapedImagesAndVideos);

module.exports = router;

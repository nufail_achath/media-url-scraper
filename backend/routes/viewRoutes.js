const express = require("express");
const {
  getScrapedImagesAndVideos,
} = require("../controllers/scraperController");
const { getScraperView } = require("../controllers/viewController");
const { isAuthenticated } = require("../controllers/authController");

const router = express.Router();
router.route("/").get(getScraperView);

module.exports = router;

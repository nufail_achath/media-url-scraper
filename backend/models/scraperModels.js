const mongoose = require("mongoose");
const elementSchema = new mongoose.Schema({
  category: {
    type: String,
    trim: true,
  },
  type: {
    type: String,
    trim: true,
  },
  src: {
    type: String,
    trim: true,
  },
});

const scraperSchema = new mongoose.Schema(
  {
    url: {
      type: String,
      required: [true, "A scrapper must have web a URL"],
    },

    images: [elementSchema],
    videos: [elementSchema],
    createdAt: {
      type: Date,
      default: Date.now(),
      select: false,
    },
  },
  {
    toJSON: {
      transform: function (doc, ret) {
        delete ret._id;
      },
    },
  }
);

const Scraper = mongoose.model("Scraper", scraperSchema);

module.exports = Scraper;

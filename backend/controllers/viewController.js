const Scraper = require("../models/scraperModels");
const catchAsync = require("./../utils/catchAsync");
const _ = require("lodash");

exports.getScraperView = catchAsync(async (req, res) => {
  const scrapers = await Scraper.find().select("images videos");
  const urls = _.flatten(scrapers.map((e) => [...e.images, ...e.videos]));
  res.status(200).render("scraper", {
    status: "success",
    title: "Media Scraper",
    urls,
    result: urls.length,
    pageSize: 4,
  });
});

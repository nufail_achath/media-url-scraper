const Scraper = require("../models/scraperModels");
const catchAsync = require("./../utils/catchAsync");
const { scrapeImageAndVideos } = require("../utils/scraper");
const _ = require("lodash");
exports.checkScraperBody = (req, res, next) => {
  if (
    !req.body.webUrls ||
    req.body.webUrls.length == 0 ||
    Object.keys(req.body).length === 0
  )
    return res.status(400).json({
      status: "fail",
      message: "Please add Web URLs",
    });
  next();
};

exports.scrapWebURLs = catchAsync(async (req, res, next) => {
  const urls = await Promise.allSettled(scrapeImageAndVideos(req.body.webUrls));
  let scrapedItems = [];
  if (urls) {
    scrapedItems = urls.map((item) => {
      if (item.status == "fulfilled") {
        return _.pick(item.value, ["url", "images", "videos"]);
      }
    });
  }
  req.scrapedItems = scrapedItems;
  next();
});

exports.createScraper = catchAsync(async (req, res, next) => {
  await Scraper.insertMany(req.scrapedItems);
  res.status(201).json({
    status: "success",
    data: {
      message: "Scuccessfuly scaraped images and videos from web URLs",
    },
  });
});

exports.getScrapedImagesAndVideos = catchAsync(async (req, res) => {
  const scrapers = await Scraper.find().select("images videos");
  const urls = _.flatten(scrapers.map((e) => [...e.images, ...e.videos]));
  res.status(200).json({
    status: "success",
    result: urls.length,
    data: { urls },
  });
});

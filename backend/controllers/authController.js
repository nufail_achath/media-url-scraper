const AppError = require("./../utils/appError");
exports.isAuthenticated = (req, res, next) => {
  if (req?.params?.role !== "admin") {
    console.log("admin ", req?.params?.role);
    console.log("Not admin");
    return next(
      new AppError("You do not have permission to perform this action", 403)
    );
  }
  next();
};

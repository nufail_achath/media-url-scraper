import React from "react";
import { Routes, Route } from "react-router-dom";
import Header from "../components/Header";
import NotFound from "../pages/NotFound";
import Scraper from "../pages/Scraper";

export default function Home() {
  return (
    <Routes>
      <Route path="/" element={<Header />}>
        <Route index element={<Scraper />} />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

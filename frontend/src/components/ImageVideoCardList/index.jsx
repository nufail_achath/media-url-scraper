import React from "react";
import Image from "../common/Image";
import Video from "../common/Video";

function ImageVideoCardList({ items }) {
  return (
    <div className="row row-cols-1 row-cols-md-3 g-4">
      {items &&
        items.map(({ src, type, category, _id }) => (
          <div className="col">
            <div className="card h-100 p-2" key={`card${_id}`}>
              {category === "image" ? (
                <Image src={src} key={`img${_id}`} />
              ) : (
                <Video src={src} type={type} key={`vdo${_id}`} />
              )}

              <div className="card-body text-white bg-secondary">
                <h6 className="card-title text-capitalize">{category}</h6>
              </div>
            </div>
          </div>
        ))}
    </div>
  );
}

export default ImageVideoCardList;

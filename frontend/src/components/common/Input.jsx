import React from "react";

function Input({ value, placeHolder, onChange }) {
  return (
    <div className="input-group input-group-sm mb-3">
      <input
        type="text"
        className="form-control"
        aria-label="Small"
        aria-describedby="inputGroup-sizing-sm"
        placeholder={placeHolder}
        value={value}
        onChange={(e) => {
          onChange(e?.target?.value);
        }}
      />
    </div>
  );
}

export default Input;

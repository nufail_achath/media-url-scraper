import React from "react";

function Image({ src }) {
  return (
    <img src={src} className="card-img-top" alt="media-scrapper" height="218" />
  );
}

export default Image;

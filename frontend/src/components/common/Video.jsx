import React from "react";

function Video({ src, type }) {
  return (
    <video controls autoPlay={false}>
      <source src={src} type={type} />
      <p>Sorry, your browser doesn't support embedded videos.</p>
    </video>
  );
}

export default Video;

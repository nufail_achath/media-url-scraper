import React from "react";

function Button({ label, onClick, classes }) {
  return (
    <button type="button" className={`btn  m-2 ${classes}`} onClick={onClick}>
      {label}
    </button>
  );
}

export default Button;

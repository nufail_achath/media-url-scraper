import React from "react";
const ListGroup = (props) => {
  const {
    items,
    textProperty,
    valueProperty,
    onItemSelect,
    selectedItem,
    title,
  } = props;
  return (
    <>
      {title}
      <ul className="list-group">
        {items.map((item) => (
          <li
            className={
              item[textProperty] === selectedItem[textProperty]
                ? "list-group-item active clickable"
                : "list-group-item clickable"
            }
            onClick={() => onItemSelect(item)}
            key={item[valueProperty]}
          >
            {item[textProperty]}
          </li>
        ))}
      </ul>
    </>
  );
};
ListGroup.defaultProps = {
  textProperty: "name",
  valueProperty: "_id",
};
export default ListGroup;

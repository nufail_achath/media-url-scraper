import React from "react";

function MessageBox({ text, classes }) {
  return (
    <div className={`alert ${classes} m-3 col-10`} role="alert">
      {text}
    </div>
  );
}

export default MessageBox;

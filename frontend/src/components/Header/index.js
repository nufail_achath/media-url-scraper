import React from "react";
import { NavLink, Outlet } from "react-router-dom";

export default function Header() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <NavLink to="/" className="navbar-brand">
            Media Scraper
          </NavLink>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <NavLink to="/" className="nav-item nav-link">
                Home
              </NavLink>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container-fluid mt-3">
        <Outlet />
      </div>
    </div>
  );
}

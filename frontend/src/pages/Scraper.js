import React, { useState } from "react";
import http from "../services/httpServices";
import { debounce } from "lodash";
import ListGroup from "../components/common/ListGroup";
import Pagination from "../components/common/Pagination";
import { paginate } from "../utils/paginate";
import ImageVideoCardList from "../components/ImageVideoCardList/index";
import Input from "../components/common/Input";
import Button from "../components/common/Button";
import MessageBox from "../components/common/MessageBox";
import {
  PAGESIZE,
  CATEGORIES,
  WEB_URLS as webUrls,
  USER_ROLE as role,
  API,
} from "../utils/config";

export default function Scraper() {
  const [pageSize] = useState(PAGESIZE);
  const [selectedCategory, setSelectedCategory] = useState(CATEGORIES[0]);
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchQuery, setSearchQuery] = useState("");
  const [message, setMessage] = useState("");
  const [scraped, setScraped] = useState(false);

  const scrapeWebUrls = async () => {
    //API CALL TO SCRAP URLs
    try {
      const apiEndPoint = `${API}/scrapers/${role}`;
      const { data, status } = await http.post(apiEndPoint, { webUrls });
      if (status === 201) {
        setScraped(true);
        setMessage(data?.data?.message);
      }
    } catch (err) {
      setMessage(err.message);
    }
  };

  const viewScrappedMediaURLs = async () => {
    //API CALL TO GET Media URLs
    try {
      const apiEndPoint = `${API}/scrapers/${role}`;
      const { data } = await http.get(apiEndPoint);
      if (!data.result) {
        setData([]);

        setMessage(`No data to display. Please scrape web urls..!.`);
        return;
      }
      setMessage("");
      setData(data?.data?.urls);
    } catch (err) {
      setMessage(err.message);
    }
  };

  //Handle Actions
  const handleSearch = debounce((value) => {
    setSearchQuery(value);
  }, 200);
  const handleCategorySelect = (type) => {
    setSelectedCategory(type);
    setCurrentPage(1);
  };
  const handlePageChnage = (page) => {
    setCurrentPage(page);
  };

  //Filter Data
  const pagedData = () => {
    const filteredData =
      selectedCategory && selectedCategory._id && selectedCategory._id !== "all"
        ? data.filter((m) => m.category === selectedCategory._id)
        : data;

    const searchedData = searchQuery
      ? filteredData.filter((item) =>
          item.category.toLowerCase().includes(searchQuery)
        )
      : filteredData;

    const items = paginate(searchedData, currentPage, pageSize);
    return { totalCount: searchedData.length, items };
  };

  //Final Filtered Data to display
  const { totalCount, items } = pagedData();

  //Render UI
  return (
    <div className="row">
      <div className="col-12">
        {!scraped && (
          <Button
            classes="btn-secondary"
            label="Scrape Web URL's"
            onClick={scrapeWebUrls}
          />
        )}
        <Button
          classes="btn-secondary"
          label="View Media"
          onClick={viewScrappedMediaURLs}
        />
      </div>

      <div className="col-2">
        <ListGroup
          items={CATEGORIES}
          onItemSelect={handleCategorySelect}
          selectedItem={selectedCategory}
          title={<h6>Category</h6>}
        />
      </div>
      <div className="col-10">
        <Input
          value={searchQuery}
          placeHolder="Search by category..."
          onChange={handleSearch}
        />

        <p>Showing {totalCount} items</p>
        {message && <MessageBox text={message} classes="alert-primary" />}
        {totalCount > 0 && (
          <>
            <ImageVideoCardList
              items={items}
              key={`scraper_items_${(Math.random() * items.length).toFixed(2)}`}
            />
            <Pagination
              itemCount={totalCount}
              pageSize={pageSize}
              onPageChange={handlePageChnage}
              currentPage={currentPage}
            />
          </>
        )}
      </div>
    </div>
  );
}

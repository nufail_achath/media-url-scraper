export const API = process.env.REACT_APP_API;
export const USER_ROLE = process.env.REACT_APP_USER_ROLE;
export const CATEGORIES = [
  { _id: "all", name: "all" },
  { _id: "image", name: "image" },
  { _id: "video", name: "video" },
];
export const WEB_URLS = [
  "https://www.shutterstock.com/video/clip-1084218295-futuristic-animated-concept-big-data-center-chief",
  "https://www.shutterstock.com/video/clip-1079039945-solar-power-plant-windmills-aerial-view-renewable",
  "https://www.shutterstock.com/video/clip-1085395847-into-metaverse-digital-technology-hitech-concept",
];
export const PAGESIZE = 6;
